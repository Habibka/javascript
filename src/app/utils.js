/**
 * Here you can define helper functions to use across your app.
 */

const getPlanetInfo = async (url) => await fetch(url).then((response) => response.json());

export default getPlanetInfo;
